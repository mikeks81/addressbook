import React, { Component } from 'react';
import Header from './Components/Header/Header'
import AddressBook from './Components/AddressBook/AdressBook'
import './App.sass'

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Header />
        <AddressBook />
      </div>
    );
  }
}

export default App;
