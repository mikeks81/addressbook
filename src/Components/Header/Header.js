import React from 'react'
import './Header.sass'

const Header = () => {
  return (
    <nav className='nav-wrapper blue-grey lighten-3'>
      <p className='left'>Address Book</p>
    </nav>
  )
}

export default Header
