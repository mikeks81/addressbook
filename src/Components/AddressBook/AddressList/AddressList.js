import React from 'react'
import PropTypes from 'prop-types'
import {Collection, CollectionItem, Icon} from 'react-materialize'
import { formatPhoneNumber } from '../../../utils/formatting'
import './AddressList.sass'

class AddressList extends React.PureComponent {
  /**
   * proxy function that returns a function with a
   * scoped "address" argument. OnClick of the button it
   * calls a function to remove that address from state.
   * @param {obj} address 
   */
  _removeAddress (address) {
    const {removeAddress} = this.props
    return (e) => {
      removeAddress(address)
    }
  }

  renderList () {
    const {data} = this.props
    return data.map(address => (
      <CollectionItem className='list-item left-align valign-wrappe hoverable' key={address.phone}>
        <span>
          <Icon>account_circle</Icon>{`${address.firstName} ${address.lastName}`}
        </span>
        <span>
          <Icon>call</Icon>{formatPhoneNumber(address.phone)}
        </span>
        <br />
        <Icon>home</Icon>{`${address.street} ${address.city} ${address.state} ${address.zip}`}
        <br />
        <Icon>email</Icon>{address.email}
        <button
          className='btn waves-effect waves-light btn-flat red darken-3 right'
          onClick={this._removeAddress(address)}
        >
          <i className='material-icons right'>delete</i>
        </button>
      </CollectionItem>

    ))
  }
  render () {
    return (
      <Collection className='address-list'>
        {this.renderList()}
      </Collection>
    );
  }
}

AddressList.propTypes = {
  data: PropTypes.array
}

AddressList.defaultProps = {
  data: []
}

export default AddressList
