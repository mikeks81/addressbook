import React from 'react'
import {Row, Col, CardPanel} from 'react-materialize'
import AddressForm from './AddressForm/AddressForm'
import AddressList from './AddressList/AddressList'
import {ADDRESSES} from '../../utils/data'
import './AddressBook.sass'
/**
 * AddressBook will be the dataStore for this small exercise
 * redux would be best for a biggger application but give the requiements
 * and the time limit I decided it isn't necessary.
 */
class AdressBook extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {data: [...ADDRESSES]}
    this.updateAdressStore = this.updateAdressStore.bind(this)
    this.removeAddress = this.removeAddress.bind(this)
  }
  /** updates the address state without wiping all past entries */
  updateAdressStore (address) {
    this.setState(state => ({ data: [...state.data, address] }))
  }

  /**
   * looks up the existing address store by phone or email
   * if found we stop the iteration and destructure the data array
   * and return a new array without the address obj passed into the argument
   * @param {obj} address 
   */
  removeAddress (address) {
    const {data} = this.state
    let match = null
    data.some(_address => {
      if (_address.phone === address.phone) {
        match = _address.phone
        return true
      } else if (_address.email === address.email) {
        match = _address.email
        return true
      }
    })

    if (match) {
      const [match, ...rest] = data
      this.setState({data: rest})
    } else {
      console.log('could not match item')
    }
  }

  render () {
    return (
      <main className='address-book'>
        <Row>
          <Col s={12} m={5}>
            <CardPanel>
              <AddressForm data={this.state.data} updateAdressStore={this.updateAdressStore} />
            </CardPanel>
          </Col>
          <Col s={12} m={5}>
            <CardPanel>
              <AddressList data={this.state.data} removeAddress={this.removeAddress} />
            </CardPanel>
          </Col>
        </Row>
      </main>
    )
  }
}

export default AdressBook
