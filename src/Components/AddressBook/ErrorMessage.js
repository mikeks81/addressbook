import React from 'react'
import PropTypes from 'prop-types'
import './ErrorMessage.sass'


const ErrorMessage = ({show, msg}) => {
  const showClass = show ? 'invalid' : ''
  return (
    <div className={`error-message ${showClass}`}>
    { msg
        ? msg
        : (
            <div>
              You must fill one of the following out to add an address:
              <br />
              <ul>
                <li>Phone</li>
                <li>Address (Street, City, State, Zip)</li>
                <li>Email</li>
              </ul>
            </div>
          )
    }
    </div>
  )
}

ErrorMessage.propTypes = {
  show: PropTypes.bool,
  msg: PropTypes.string
}

export default ErrorMessage
