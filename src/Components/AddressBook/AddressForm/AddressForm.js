import React from 'react'
import PropTypes from 'prop-types'
import {Button, Row, Input, Icon} from 'react-materialize'
import ErrorMessage from '../ErrorMessage'
/**
 * defining initial form state
 */
const defaultInputValues = {
  firstName: '',
  lastName: '',
  street: '',
  city: '',
  state: '',
  zip: '',
  email: '',
  phone: ''
}

class AddressForm extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {...defaultInputValues, formErrors: false, errorMsg: ''}
    this.updateKV = this.updateKV.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  /**
   * updating all input by name and value
   */
  updateKV (e) {
    const {name, value} = e.target
    this.setState(prevState => ({[name]: value}))
  }

  /**
   * onSubmit calls for valiation and if validation clears
   * it update the address store.
   * @param {obj} e 
   */
  handleSubmit (e) {
    e.preventDefault()
    const {updateAdressStore} = this.props
    const {formErrors, ...state} = this.state
    const isValidForm = this.formValidation()

    if (isValidForm) {
      updateAdressStore(state)
      /** clearing inputs after a save */
      this.clearInputs()
    }
  }
  /**
   * resets all the inputValues to original state
   */
  clearInputs () {
    this.setState({...defaultInputValues})
  }

  /**
   * checking for any email or phone duplicates
   * updating state with specific error and
   * returning a bool 
   */
  validateUniqueness () {
    const {data} = this.props
    const {phone, email} = this.state
    // let errors = {phone: false, email: false}
    let errorMsg = ''
    let error = null

    /** finding a duplicate match and stopping iteration and marking the error */
    data.some(address => {
      if (phone === address.phone) {
        error = 'phone'
        return true
      } else if (email === address.email) {
        error = 'email'
        return true
      }
    })

    if (error) {
      errorMsg = `The ${error} you entered has already been added`
      this.setState({ errorMsg, formErrors: true })
      return true
    } else {
      return false
    }
  }

  /**
   * reseting error statuses before any validation happens
   * so that old state doesn't skew the validation.
   */
  resetFormErrors () {
    this.setState({ errorMsg: '', formErrors: false })
  }

  /**
   * Checks for empty form fields or duplicates of phone or email
   */
  formValidation () {
    this.resetFormErrors()
    const hasNoEmpties = this.validateEmptyFields()
    let hasDuplicates = false
    if (!hasNoEmpties) {
      this.setState({ formErrors: true })
    } else {
      /**
       * only running uniquness validation if at least one required
       * field is filled out
       */
      hasDuplicates = this.validateUniqueness()
    }

    return hasNoEmpties && !hasDuplicates
  }

  /**
   * checks for teh specified fields to see if they're valid or not.
   */
  validateEmptyFields () {
    const { phone, email, street, city, state, zip } = this.state
    const isAddress = Boolean(street) && Boolean(city) && Boolean(state) && Boolean(zip)
    const isPhone = Boolean(phone)
    const isEmail = Boolean(email)
    const isValid = isAddress || isPhone || isEmail
    return isValid
  }

  render () {
    const {
      firstName,
      lastName,
      street,
      city,
      state,
      zip,
      email,
      phone,
      formErrors,
      errorMsg
    } = this.state
    return (
      <React.Fragment>
        <ErrorMessage show={formErrors} msg={errorMsg} />
        <Row>
          <Input
            s={6}
            name='firstName'
            label='First Name'
            onChange={this.updateKV}
            value={firstName}
          >
            <Icon>account_circle</Icon>
          </Input>
          <Input
            s={6}
            name='lastName'
            label='Last Name'
            onChange={this.updateKV}
            value={lastName}
          >
            <Icon>phone</Icon>
          </Input>
        </Row>
        <Row>
          <Input
            s={8}
            name='street'
            label='Street Number'
            onChange={this.updateKV}
            value={street}
          >
            <Icon>home</Icon>
          </Input>
          <Input
            s={4}
            name='city'
            label='City'
            onChange={this.updateKV}
            value={city}
          />
          <Input
            s={8}
            name='state'
            label='State'
            onChange={this.updateKV}
            value={state}
          >
            <Icon>home</Icon>
          </Input>
          <Input
            s={4}
            name='zip'
            label='Zip'
            onChange={this.updateKV}
            value={zip}
          />
        </Row>

        <Row>
          <Input
            s={6}
            name='phone'
            label='Phone Number'
            validate
            onChange={this.updateKV}
            value={phone}
          >
            <Icon>phone</Icon>
          </Input>
          <Input
            s={6}
            name='email'
            label='Email'
            validate
            type='email'
            onChange={this.updateKV}
            value={email}
          >
            <Icon>email</Icon>
          </Input>
        </Row>
        <Button waves='light' onClick={this.handleSubmit}>
          Add Address
          <Icon right>person_add</Icon>
        </Button>
      </React.Fragment>
    )
  }
}

AddressForm.propTypes = {
  updateAdressStore: PropTypes.func
}

export default AddressForm
